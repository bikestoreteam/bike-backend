package com.bikestore.bikebackend.service.dto;

import com.bikestore.bikebackend.domain.Users;
import com.sun.istack.NotNull;

import java.time.LocalDate;

public class InvoiceDTO {
    @NotNull
    private Long id;
    private LocalDate date;
    private Double total;
    private String observations;
    private Users client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public Users getClient() {
        return client;
    }

    public void setClient(Users client) {
        this.client = client;
    }
}
