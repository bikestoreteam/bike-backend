package com.bikestore.bikebackend.service.dto;

import com.bikestore.bikebackend.domain.Bike;
import com.bikestore.bikebackend.domain.Invoice;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetailInvoiceDTO {

    @NotNull
    private Long id;
    private Integer quantity;
    private Double price;
    private Double tax;
    private float discount;
    private Bike bike;
    private Invoice invoice;
}
