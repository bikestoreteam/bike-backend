package com.bikestore.bikebackend.service.dto;

import com.bikestore.bikebackend.domain.enumeration.ShockAbsorber;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BikeDTO {

    @NotNull
    private Long id;

    private String model;
    private String serial;
    private Double price;
    private ShockAbsorber shockAbsorber;

}
