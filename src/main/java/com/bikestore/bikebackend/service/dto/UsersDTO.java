package com.bikestore.bikebackend.service.dto;

import com.bikestore.bikebackend.domain.enumeration.Gender;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class UsersDTO {
    @NotNull
    private Long id;
    private String documentNumber;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private Gender gender;
    private Set<Rols> rols;
}
