package com.bikestore.bikebackend.repository;

import com.bikestore.bikebackend.domain.Bike;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BikeRepository extends JpaRepository<Bike, Long> {

}
