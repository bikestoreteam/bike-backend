package com.bikestore.bikebackend.repository;

import com.bikestore.bikebackend.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Long> {
}
