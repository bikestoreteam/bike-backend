package com.bikestore.bikebackend.repository;

import com.bikestore.bikebackend.domain.DetailInvoice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DetailInvoiceRepository extends JpaRepository<DetailInvoice, Long> {

}
