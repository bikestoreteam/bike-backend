package com.bikestore.bikebackend.repository;

import com.bikestore.bikebackend.domain.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
}
