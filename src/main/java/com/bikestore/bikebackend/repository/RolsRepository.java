package com.bikestore.bikebackend.repository;

import com.bikestore.bikebackend.domain.Rols;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolsRepository extends JpaRepository<Rols, Long> {

}
