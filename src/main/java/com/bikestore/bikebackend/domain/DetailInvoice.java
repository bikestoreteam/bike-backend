package com.bikestore.bikebackend.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DetailInvoice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long id;

    private Integer quantity;

    private Double price;

    private Double tax;

    private float discount;

    @ManyToOne
    private Bike bike;

    @ManyToOne
    private Invoice invoice;

}
