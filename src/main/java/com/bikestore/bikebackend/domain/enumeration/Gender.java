package com.bikestore.bikebackend.domain.enumeration;

public enum Gender {
    M, F
}
