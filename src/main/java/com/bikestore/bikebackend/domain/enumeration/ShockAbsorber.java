package com.bikestore.bikebackend.domain.enumeration;

public enum ShockAbsorber {

    HARDTAIL, FULL_SUSPENSION, RIGID
}
